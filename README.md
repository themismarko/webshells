# Webshells

A collection of custom webshells used during projects

## webshell.aspx
AV / EDR and Application Whitelisting bypass using Runspaces

### use

`
http://<target>/webshell.aspx?cmd=<command>
`

